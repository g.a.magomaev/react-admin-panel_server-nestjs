import { Body, Controller, Post } from '@nestjs/common';
import { IResponseMessage } from 'src/interfaces/response-message.interface';
import { AuthService } from './auth.service';
import { UserDto } from './dto/user.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/register')
  registerUser(@Body() registerUserDto: UserDto): Promise<IResponseMessage> {
    return this.authService.registerUser(registerUserDto);
  }

  @Post('/login')
  loginUser(@Body() registerUserDto: UserDto): Promise<IResponseMessage> {
    return this.authService.loginUser(registerUserDto);
  }
}
