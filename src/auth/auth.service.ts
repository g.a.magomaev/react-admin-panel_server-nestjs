import { HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IResponseMessage } from 'src/interfaces/response-message.interface';
import { User, UserDocument } from '../schemas/user.schema';
import { UserDto } from './dto/user.dto';
import { IUser } from './interfaces/user.interface';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    private readonly jwtService: JwtService,
  ) {}

  async registerUser(userDto: UserDto): Promise<IResponseMessage> {
    const { email, password }: UserDto = userDto;
    const hasMatches: UserDocument = await this.findUserByEmail(email);
    if (hasMatches) {
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        message: [`The user with email: ${email} is exist.`],
      };
    }

    const newUser: IUser = { email, password, id: await this.getActualId() };

    await this.userModel.collection.insertOne(newUser, () => ({
      statusCode: HttpStatus.CREATED,
      message: [`The user with email: ${email} was registered.`],
    }));

    return {
      statusCode: HttpStatus.BAD_REQUEST,
      message: [`Something wrong`],
    };
  }

  async loginUser(userDto: UserDto): Promise<IResponseMessage> {
    const { email, password }: UserDto = userDto;
    const dataBaseUser: UserDocument = await this.findUserByEmail(email);

    if (!dataBaseUser) {
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        message: [`The user with email: ${email} isn't exist.`],
      };
    }

    if (dataBaseUser.password !== password) {
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        message: ['The password is incorrect. Try again.'],
      };
    }

    return {
      statusCode: 200,
      message: ['Access is allowed'],
      data: {
        id: dataBaseUser.id as number,
        token: this.jwtService.sign({ email, password }),
      },
    };
  }

  private async findUserByEmail(email: string): Promise<UserDocument> {
    const [user] = await this.userModel.find({ email });
    return user;
  }

  private async getActualId(): Promise<number> {
    const [lastUser] = await this.userModel.find().sort({ id: -1 }).limit(1);

    return lastUser ? lastUser.id + 1 : 1;
  }
}
