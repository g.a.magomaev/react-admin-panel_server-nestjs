import { Transform } from 'class-transformer';
import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MinLength,
  ValidationArguments,
} from 'class-validator';

export class UserDto {
  @IsString()
  @Transform((value: any) => value.trim())
  @IsNotEmpty({
    message: (args: ValidationArguments) =>
      `The field: '${args.property}' is required`,
  })
  @IsEmail()
  readonly email: string;

  @IsString()
  @Transform((value: any) => value.trim())
  @IsNotEmpty({
    message: (args: ValidationArguments) =>
      `The field: '${args.property}' is required`,
  })
  @MinLength(6, {
    message: 'The password min length must be more than six symbols',
  })
  readonly password: string;

  readonly id?: number;
}
