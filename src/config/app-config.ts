import { registerAs } from '@nestjs/config';

export default registerAs('app', () => ({
  port: 5000,
}));
