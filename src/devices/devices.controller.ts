import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { IResponseMessage } from 'src/interfaces/response-message.interface';
import { DeviceDocument } from 'src/schemas/device.schema';
import { DevicesService } from './devices.service';
import { IDevice } from './interfaces/device.interface';
import { IDevicesQuery } from './interfaces/devices-query.interface';

@Controller('')
export class DevicesController {
  constructor(private readonly devicesService: DevicesService) {}

  @Get('devices')
  findAllDevices(@Query() query: IDevicesQuery): Promise<DeviceDocument[]> {
    return this.devicesService.getDevices(query);
  }

  @Post('device')
  createDevice(@Body() newDevice: IDevice): Promise<IResponseMessage> {
    return this.devicesService.createDevice(newDevice);
  }

  @Put('device')
  updateDevice(@Body() newDevice: IDevice): Promise<IResponseMessage | void> {
    return this.devicesService.updateDevice(newDevice);
  }

  @Delete('device/:id')
  deleteDevice(@Param() { id }: { id: number }): Promise<IResponseMessage> {
    return this.devicesService.deleteDevice(id);
  }
}
