import {
    HttpStatus,
    Injectable,
}                           from '@nestjs/common';
import { InjectModel }      from '@nestjs/mongoose';
import {
    CallbackError,
    Model,
}                           from 'mongoose';
import { IResponseMessage } from 'src/interfaces/response-message.interface';
import {
    Device,
    DeviceDocument,
}                           from 'src/schemas/device.schema';
import { Utils }            from 'src/utilities/utils';
import { ISortProps }       from "../interfaces/sort-props.interface";
import { IDevice }          from './interfaces/device.interface';
import { IDevicesQuery }    from './interfaces/devices-query.interface';


@Injectable()
export class DevicesService {
    constructor (
        @InjectModel( Device.name ) private deviceModel: Model<DeviceDocument>,
    ) {
    }

    async getDevices ( {
        search,
        filter,
        sort,
    }: IDevicesQuery ): Promise<DeviceDocument[]> {
        if ( search ) {
            return this.deviceModel.find( {
                name: { $regex: search, $options: 'ig' },
            } );
        } else if ( filter ) {
            return this.deviceModel.find(
                Utils.getMongoDBFilterParams( filter as string ),
            );
        } else if ( sort ) {
            const { order, sortBy }: ISortProps = JSON.parse( sort );
            return this.deviceModel.find().sort( { [sortBy]: order.toString() } );
        }

        return this.deviceModel.find();
    }

    async createDevice ( {
        id,
        name,
        room_id,
        type,
        description,
    }: IDevice ): Promise<IResponseMessage> {
        const hasDevice: DeviceDocument | null = await this.deviceModel.findOne( {
            name,
        } );

        if ( hasDevice ) {
            return {
                statusCode: HttpStatus.BAD_REQUEST,
                message:    [ `The device with name: ${ name } is exist.` ],
            };
        }

        const [ lastDevice ]: DeviceDocument[] = await this.deviceModel
                                                           .find()
                                                           .sort( { id: -1 } )
                                                           .limit( 1 );

        id = lastDevice ? lastDevice.id + 1 : 1;

        await this.deviceModel.collection.insertOne(
            { id, name, room_id, type, description },
            ( err: CallbackError ) => {
                if ( err ) {
                    return {
                        statusCode: HttpStatus.BAD_REQUEST,
                        message:    [ err.message ],
                    };
                }
            },
        );
        return {
            statusCode: HttpStatus.CREATED,
            message:    [ `The device with name: ${ name } was added.` ],
        };
    }

    async updateDevice ( {
        id,
        name,
        room_id,
        type,
        description,
    }: IDevice ): Promise<IResponseMessage | void> {
        const editingDevice: DeviceDocument | null = await this.deviceModel.findOne(
            {
                id,
            },
        );
        if ( !editingDevice ) {
            return {
                statusCode: HttpStatus.BAD_REQUEST,
                message:    [
                    `The device with id: ${ id } is not exist, maybe it was already deleted by another user`,
                ],
            };
        }
        const hasMatchesDeviceName:
            | DeviceDocument[]
            | null = await this.deviceModel.find( { name } );

        if ( hasMatchesDeviceName?.length > 1 ) {
            return {
                statusCode: HttpStatus.BAD_REQUEST,
                message:    [ `The device with name: '${ name }' is exist.` ],
            };
        }

        editingDevice.update(
            {
                id,
                name,
                room_id: room_id.toString(),
                type,
                description,
            },
            {},
            ( err: CallbackError ) => {
                if ( err ) {
                    return {
                        statusCode: HttpStatus.BAD_REQUEST,
                        message:    [ err.message ],
                    };
                }
            },
        );
        return {
            statusCode: 200,
            message:    [ `The device '${ name }' was updated` ],
        };
    }

    async deleteDevice ( id: number ): Promise<IResponseMessage> {
        const hasDevice: DeviceDocument | null = await this.deviceModel.findOne( {
            id,
        } );

        if ( hasDevice ) {
            await this.deviceModel.deleteOne( { id } );
            return {
                statusCode: HttpStatus.CREATED,
                message:    [ `The device '${ hasDevice.name }' with id: ${ id } was deleted.` ],
            };
        } else {
            return {
                statusCode: HttpStatus.BAD_REQUEST,
                message:    [
                    `The device with id: ${ id } is not exist, maybe it was already deleted by another user`,
                ],
            };
        }
    }
}
