import { Transform } from "class-transformer";
import {
    IsInt,
    IsNotEmpty,
    IsString,
    Min,
    ValidationArguments,
}                    from 'class-validator';


export class DeviceDto {
    @IsInt()
    readonly id?: number;

    @IsInt()
    @Transform( ( value: any ) => value.trim() )
    @IsNotEmpty( {
        message: ( args: ValidationArguments ) =>
                     `The field: '${ args.property }' is required`,
    } )
    @Min( 1 )
    readonly room_id: number;

    @IsString()
    @Transform( ( value: any ) => value.trim() )
    @IsNotEmpty( {
        message: ( args: ValidationArguments ) =>
                     `The field: '${ args.property }' is required`,
    } )
    readonly name: string;

    @IsString()
    @Transform( ( value: any ) => value.trim() )
    @IsNotEmpty( {
        message: ( args: ValidationArguments ) =>
                     `The field: '${ args.property }' is required`,
    } )
    readonly type: string;

    @IsString()
    @Transform( ( value: any ) => value.trim() )
    readonly description: string;
}
