export type IDevice = {
  id: number;
  room_id: number;
  name: string;
  type: string;
  description?: string;
};
