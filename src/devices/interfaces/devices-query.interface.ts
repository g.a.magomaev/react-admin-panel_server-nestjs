export interface IDevicesQuery {
    search?: string;
    filter?: string;
    sort?: string;
}
