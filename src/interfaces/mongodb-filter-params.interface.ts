export type IMongoDBFilterParams<T> = {
  [key: string]: { $in: T };
};
