import { HttpStatus } from '@nestjs/common';
import { IUser } from 'src/auth/interfaces/user.interface';

export interface IResponseMessage {
  statusCode: HttpStatus;
  message: string[];
  error?: string;
  data?: IUser;
}
