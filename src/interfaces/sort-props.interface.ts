export interface ISortProps {
    sortBy: string,
    order: number
}
