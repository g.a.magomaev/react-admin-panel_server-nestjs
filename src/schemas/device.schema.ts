import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type DeviceDocument = Device & Document<Device>;

@Schema()
export class Device {
  @Prop()
  id: number;

  @Prop()
  room_id: string;

  @Prop()
  name: string;

  @Prop()
  type: string;

  @Prop()
  description: string;
}

export const DeviceSchema = SchemaFactory.createForClass(Device);
