import { IMongoDBFilterParams } from 'src/interfaces/mongodb-filter-params.interface';

export class Utils {
  static getMongoDBFilterParams(filterParamsStr: string) {
    const filterParams: { [key: string]: string[] | number[] } = JSON.parse(
      filterParamsStr,
    );
    const mongoDBFilterParams: IMongoDBFilterParams<string[] | number[]> = {};
    Object.keys(filterParams).forEach((key: string) => {
      mongoDBFilterParams[key] = { $in: filterParams[key] };
    });
    return mongoDBFilterParams;
  }
}
